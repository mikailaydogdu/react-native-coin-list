import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Ionicons } from '@expo/vector-icons'
import FavoriPage from './Screen/Favori';
import DetailsPage from './Screen/Details';
import HomePage from './Screen/Home';
import Allcoin from './components/Allcoin';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import 'react-native-gesture-handler';
import ProfileTabs from './Screen/ProfileTabs';
import CategoryPage from './Screen/Category';
import CategoryListPage from './components/categoryList';
import NotificationPage from './Screen/Notification';
import ExpiredCoinPage from './Screen/ExpiredCoin';
import IndexPage from './Screen/İndex';

const Tab = createMaterialTopTabNavigator();
const HomeScreen = () => {
  return (
    <Tab.Navigator
      screenOptions={() => ({
        tabBarStyle: {
          backgroundColor: 'black',
          height: 50,
        },
        tabBarActiveTintColor: 'blue',
        tabBarInactiveTintColor: 'white',
      })}
    >
      <Tab.Screen name="New coins" component={HomePage} options={{ headerShown: false }} />
      <Tab.Screen name="Expired" initialRouteName="Expired" component={ExpiredCoinPage} options={{ headerShown: false, tabBarLabel:' Yayınlandı' }} />
      <Tab.Screen name="All coin" component={Allcoin} options={{ headerShown: false }} />
    </Tab.Navigator>
  )
}

const FavoriScreen = () => {
  return (
    <FavoriPage />
  )
}

const CategoryScreen = () => {
  return (
    <CategoryPage />
  )
}

const NotificationScreen = () => {
  return (
    <NotificationPage />
  )
}

const DataStack = createNativeStackNavigator()
const DataScreen = () => {
  return (
    <DataStack.Navigator>
      <DataStack.Screen name='Coins' component={HomeScreen} options={{headerStyle: { backgroundColor:'black'}, headerTintColor: 'white',}}/>
      <DataStack.Screen name='Details' component={DetailsPage} options={{headerStyle: { backgroundColor:'black'}, headerTintColor: 'white',}} />
      <DataStack.Screen name='market list' component={CategoryListPage} options={{headerStyle: { backgroundColor:'black'}, headerTintColor: 'white',}} />
      <DataStack.Screen name='yayınlandı' component={ExpiredCoinPage} options={{headerStyle: { backgroundColor:'black'}, headerTintColor: 'white',}} />
    </DataStack.Navigator>
  )
}
const IndexDataStack = createNativeStackNavigator()
const IndexDataScreen = () => {
  return (
    <IndexDataStack.Navigator>
      <IndexDataStack.Screen name='Home' component={IndexPage} options={{headerStyle: { backgroundColor:'black'}, headerTintColor: 'white',}} />
      <IndexDataStack.Screen name='Details' component={DetailsPage} options={{headerStyle: { backgroundColor:'black'}, headerTintColor: 'white',}} />
    </IndexDataStack.Navigator>
  )
}

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator
    screenOptions={({ route }) => ({
      tabBarShowLabel:false,
      tabBarStyle: {
        display:'flex',
        height:70,
        backgroundColor: 'black',
      },
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === 'HomeTabs') {
          iconName = focused
            ? 'stats-chart'
            : 'stats-chart-outline';
        } 
        else if (route.name === 'İndex') {
          iconName = focused ? 'home' : 'home-outline';
        }
        else if (route.name === 'Settings') {
          iconName = focused ? 'settings' : 'settings-outline';
        }
        else if (route.name === 'Favori') {
          iconName = focused ? 'star' : 'star-outline';
        }
        else if (route.name === 'Profile') {
          iconName = focused ? 'cog' : 'cog-outline';
        }
        else if (route.name === 'Market') {
          iconName = focused ? 'apps' : 'apps-outline';
        }
        else if (route.name === 'Notification') {
          iconName = focused ? 'notifications' : 'notifications-outline';
        }
        return <Ionicons name={iconName} size={size} color={color} />;
      },
      tabBarActiveTintColor: 'blue',
      tabBarInactiveTintColor: 'white',
    })}
  >
    <Tabs.Screen name='İndex' component={IndexDataScreen} options={{headerShown: false,headerStyle: { backgroundColor:'black'}, headerTintColor: 'white',}} />
    <Tabs.Screen name='HomeTabs' component={DataScreen} options={{headerShown: false,  headerStyle: { backgroundColor:'black'}, headerTintColor: 'white',}} />
    <Tabs.Screen name='Favori' component={FavoriScreen} options={{title: 'Favoriler', headerStyle: { backgroundColor:'black'}, headerTintColor: 'white',} } />
    <Tabs.Screen name='Notification' component={NotificationScreen} options={{headerStyle: { backgroundColor:'black'}, headerTintColor: 'white',}}/>
    <Tabs.Screen name='Profile' component={ProfileTabs}  options={{headerStyle:{backgroundColor:'black'}, headerTintColor: 'white'}}/>
  </Tabs.Navigator>
)
//    <Tabs.Screen name='Market' component={CategoryScreen}  options={{headerStyle: { backgroundColor:'black'}, headerTintColor: 'white'}} />

const MainStack = createNativeStackNavigator()
const App = () => {
  return (
    <NavigationContainer>
      <MainStack.Navigator >
        <MainStack.Screen name='Tabs' component={TabsScreen} options={{ headerShown: false }} />
      </MainStack.Navigator>
    </NavigationContainer>
  );
}



export default App;
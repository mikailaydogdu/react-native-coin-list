import React, {useState, useEffect} from 'react';
import { render } from 'react-dom';
import {FlatList, Text} from 'react-native';
import { View, StyleSheet, ScrollView, Dimensions, Image } from 'react-native';
import { format } from "date-fns";

const { width } = Dimensions.get('window');


const ExpiredPage = () => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const getCoin = async () => {
      try {
        const response = await fetch('{domain}');
        const json = await response.json();
        setData(json);
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false);
      }
    }
    useEffect(() => {
      getCoin();
    }, []);

    return (
        <View style={styles.container}>
            <FlatList
                pagingEnabled={true}
                horizontal= {true}
                snapToAlignment={"center"}
                data={data}
                enableEmptySections={true}
                keyExtractor={(item) => {
                  return item.id;
                }}
                renderItem={({ item }) => {
                  const date = new Date(item.date);
                  const a = format(date, 'dd');
                  const b = format(Date.now(), 'dd');
                  const datetime =a-b;
                  if(datetime<1){
                    return(
                      <View style={styles.view} >
                          <Image style={styles.icon} source={{ uri: item.src }} />
                          <Text style={styles.text}>{item.name}</Text>
                          <Text style={styles.text}>{datetime} önce</Text>
                      </View>
                      )
                    }else{
                  
                 
                }}}/>
               
            </View>
    );

}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'black'
    },
    view: {
        backgroundColor: '#eee',
        width:100,
        margin: 5,
        height: 100,
        borderRadius: 10,
        justifyContent : 'center',
        alignItems : 'center',
    },
    icon:{
        width: 30,
        height: 30,
    },
    text : {
        fontSize : 10,
        fontWeight : 'bold',
        color : 'green',
    },
    headertitle:{
        color:'white',
        fontSize:20,
    }
});
export default ExpiredPage;

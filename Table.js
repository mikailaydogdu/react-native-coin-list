import * as React from 'react';
import { DataTable } from 'react-native-paper';
import coinData from './Data';
import {
    FlatList,
} from 'react-native';


const optionsPerPage = [2, 3, 4];

const MyComponent = () => {
    const [page, setPage] = React.useState(1);
    const [itemsPerPage, setItemsPerPage] = React.useState(optionsPerPage[1]);

    React.useEffect(() => {
        setPage(1);
    }, [itemsPerPage]);

    return (
        <DataTable style={{ flex: 1 }}>
            <DataTable.Header>
              <DataTable.Title>name</DataTable.Title>
              <DataTable.Title>datatime</DataTable.Title>
              <DataTable.Title>price</DataTable.Title>
              <DataTable.Title>Beğen</DataTable.Title>
            </DataTable.Header>
            <FlatList
                enableEmptySections={true}
                data={coinData}
                keyExtractor={(item) => {
                    return item.id;
                }}
                renderItem={({ item }) => {
                    return (
                        <DataTable.Header>
                            <DataTable.Title>{item.name}</DataTable.Title>
                            <DataTable.Title numeric>{item.datatime}</DataTable.Title>
                            <DataTable.Title numeric>{item.price}</DataTable.Title>
                            <DataTable.Title numeric>Beğen</DataTable.Title>
                        </DataTable.Header>
                    )
                }} />


            <DataTable.Pagination
                page={page}
                numberOfPages={5}
                onPageChange={(page) => setPage(page)}
                label={page}
                optionsPerPage={optionsPerPage}
                itemsPerPage={itemsPerPage}
                setItemsPerPage={setItemsPerPage}
                showFastPagination
                optionsLabel={'Rows per page'}
            />
        </DataTable>
    );
}

export default MyComponent;
import React, { Component, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  Button,
  SearchBar,
  SafeAreaView,
  TouchableHighlight,
  Alert, Modal, Pressable, Dimensions,
} from 'react-native';
import coinData from '../Data';
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons'
import { color } from 'react-native-reanimated';



const HelpPage = ({ screenName }) => {
  const navigation = useNavigation();
  screenName = "Details"
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <View style={styles.container}>
      <FlatList
        style={styles.notificationList}
        enableEmptySections={true}
        data={coinData}
        keyExtractor={(item) => {
          return item.id;
        }}
        renderItem={({ item }) => {
          return (
              <View style={styles.notificationBox} >
                <Image style={styles.icon}
                  source={{ uri: item.icon }} />
                <Text style={styles.name}>{item.name}</Text>
                <Text style={styles.name}>{item.price}</Text>
                <Text style={styles.name}>{item.datatime}</Text>
                <Ionicons style={styles.iconfavori} name="chevron-forward-outline" size={20} color={"black"} />
              </View>
          )
        }} />
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DCDCDC',
  },
  btnlist: {
    padding: 15,
    marginTop: 5,
    marginStart: 15,
    marginBottom: 5,
    flexDirection: 'row',
    borderRadius: 10,

  },

  btn: {
    padding: 15,
    marginTop: 5,
    marginStart: 15,
    marginBottom: 5,
    backgroundColor: 'white',
    flexDirection: 'row',
    borderRadius: 10,
    width: 200,
    alignItems: "center",
    justifyContent: "center",
  },

  notificationList: {
    padding: 10,
  },
  notificationBox: {
    padding: 15,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    borderRadius: 10,
  },
  icon: {
    width: 45,
    height: 45,
    marginRight: 5,

  },
  iconfavori: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  name: {
    flex: 1,
    marginStart: 5,
    paddingVertical: 10,
    fontSize: 14,
    color: "black",
    marginLeft: 10,
  },
  divider: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  }
});

export default HelpPage;
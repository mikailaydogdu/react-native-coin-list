import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Ionicons } from '@expo/vector-icons'

const ProfilePage = () => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.headerContent}>
          <Image style={styles.avatar} source={{ uri: 'https://www.inovex.de/wp-content/uploads/2022/01/one-year-of-react-native-1500x880.png' }} />
          <Text style={styles.name}>Conin List</Text>
        </View>
      </View>

      <View style={styles.body}>
        <View style={styles.box}>
          <Ionicons name="settings" size={30} color={"blue"} />
          <Text style={styles.username}>Account</Text>
        </View>
        <View style={styles.box}>
          <Ionicons name="create" size={30} color={"green"} />
          <Text style={styles.username}>Edit Profile</Text>
        </View>
        <View style={styles.box}>
          <Ionicons name="power" size={30} color={"red"} />
          <Text style={styles.username}>Delete account</Text>
        </View>
        <View style={styles.box}>
          <Ionicons style={styles.iconfavori} name="logo-instagram" size={30} color={"red"} />
          <Ionicons style={styles.iconfavori} name="logo-twitter" size={30} color={"darkblue"} />
          <Ionicons style={styles.iconfavori} name="logo-facebook" size={30} color={"blue"} />
        </View>
      </View>
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
  },
  header: {
    backgroundColor: "gray",
  },
  headerContent: {
    padding: 30,
    alignItems: 'center',
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "#FFFFFF",
    marginBottom: 10,
  },
  iconfavori: {
    marginStart: 20,
    flex: 1,
  },
  image: {
    width: 60,
    height: 60,
  },
  name: {
    fontSize: 22,
    color: "white",
    fontWeight: '600',
  },
  body: {
    padding: 30,
  },
  box: {
    borderRadius: 63,
    padding: 15,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: 'black',
    flexDirection: 'row',
    shadowColor: 'black',
    shadowOpacity: .2,
    shadowOffset: {
      height: 4,
      width: -4
    },
    elevation: 2
  },
  username: {
    color: "white",
    fontSize: 22,
    alignSelf: 'center',
    marginLeft: 10
  }
});

export default ProfilePage;
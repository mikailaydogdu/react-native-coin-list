import React, { Component  } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  Button,
  SearchBar,
  SafeAreaView,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {Ionicons} from '@expo/vector-icons'
import coinMarket from '../market'

const MarketList =({screenName}) => {
    const navigation = useNavigation();
    screenName="market list"
    return (
      <View style={styles.container}>
        <FlatList 
          style={styles.notificationList} 
          enableEmptySections={true}
          data={coinMarket}
          keyExtractor= {(item) => {
            return item.id;
          }}
          renderItem={({item}) => {
            return (
                <TouchableOpacity  onPress={() => navigation.navigate(screenName, {
                    name: `${item.name}`,
                    id: `${item.id}`,
                    })}>
              <View style={styles.notificationBox} >
                <Image style={styles.icon}
                  source={{uri: item.icon}}/>
                <Text style={styles.name}>{item.name}</Text>
                <Ionicons style={styles.iconfavori} name="chevron-forward-outline" size={20} color={"black"}/>
              </View>
              </TouchableOpacity>
            )}}/>
            
            
      </View>
    );
  }


const styles = StyleSheet.create({
  container:{
    backgroundColor:'#DCDCDC',

  },

  notificationList:{
    padding:10,
  },
  notificationBox: {
    padding:15,
    marginTop:5,
    marginBottom:5,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    borderRadius:10,
  },
  icon:{
    width:45,
    height:45,

  },
  iconfavori:{
    alignItems:"flex-end",
    justifyContent:"flex-end",
    paddingHorizontal:10,
    paddingVertical:10,
  },
  name:{
    flex:1,
    marginStart:5,
    paddingVertical:10,
    fontSize:14,
    color: "black",
    marginLeft:10,
  },
  divider:{
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  }
});

export default MarketList;
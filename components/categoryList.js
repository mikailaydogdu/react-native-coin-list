import React, { Component  } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  Button,
  SearchBar,
  SafeAreaView,
} from 'react-native';
import coinData from '../Data';
import { useNavigation } from '@react-navigation/native';
import {Ionicons} from '@expo/vector-icons';
import { useRoute } from "@react-navigation/native";

const CategoryListPage =({screenName}) => {
    const navigation = useNavigation();
    screenName="Details"
    const route = useRoute();
    return (
      <View style={styles.container}>
        <View style={styles.notificationList}>
        {coinData.filter(category => category.marketkey == `${route.params.key}`).map(filteredcategory => (
                <TouchableOpacity onPress={() => navigation.navigate(screenName, {
                    name: `${filteredcategory.name}`,
                    datatime: `${filteredcategory.datatime}`,
                    icon: `${filteredcategory.icon}`,
                    price: `${filteredcategory.price}`,
                    fullname: `${filteredcategory.fullname}`,
                    market: `${filteredcategory.market}`,
                    marketlink: `${filteredcategory.marketlink}`,
                    })}>
              <View style={styles.notificationBox}>
                <Image style={styles.icon}
                  source={{uri: filteredcategory.icon}}/>
                <Text style={styles.name}>{filteredcategory.name}</Text>
                <Text style={styles.name}>{filteredcategory.price}</Text>
                <Text style={styles.name}>{filteredcategory.datatime}</Text>
                <Ionicons style={styles.iconfavori} name="chevron-forward-outline" size={20} color={"black"}/>
              </View>
            
              </TouchableOpacity>
                 ))}
            </View>
            
            
      </View>
    );
  }


const styles = StyleSheet.create({
  container:{
    backgroundColor:'#DCDCDC',

  },

  notificationList:{
    padding:10,
  },
  notificationBox: {
    padding:15,
    marginTop:5,
    marginBottom:5,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    borderRadius:10,
  },
  icon:{
    width:45,
    height:45,

  },
  iconfavori:{
    alignItems:"flex-end",
    justifyContent:"flex-end",
    paddingHorizontal:10,
    paddingVertical:10,
  },
  name:{
    flex:1,
    marginStart:5,
    paddingVertical:10,
    fontSize:14,
    color: "black",
    marginLeft:10,
  },
  divider:{
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  }
});

export default CategoryListPage;
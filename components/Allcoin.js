import React, { Component, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
  Modal, Pressable, Dimensions,
} from 'react-native';
import coinData from '../Data';
import coinData1 from '../Data1';
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons'
import coinmarket from '../market'

const Allcoin = ({ screenName, screenMarket }) => {
  const navigation = useNavigation();
  screenName = "Details"
  screenMarket="market list"
  const [modalVisible, setModalVisible] = useState(false);
  const [data, setData] = useState(coinData);
 

const filterData = (filter) =>{
if(filter==='yükselenler'){
  setData(coinData1);
}else{
  setData(coinData);
}
}

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.CategoryBox}>
        <Pressable style={styles.CategoryBoxname} onPress={() => setModalVisible(true)}>
          <View>
            <Text style={{color:'white'}}>Market</Text>
          </View>
        </Pressable> 
        <TouchableOpacity onPress={()=>filterData('yükselenler')}  style={styles.CategoryBoxname}>
          <View>
            <Text style={{color:'white'}}>yükselenler {data.length}</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>filterData('Düşenler')} style={styles.CategoryBoxname}>
        <View >
          <Text style={{color:'white'}}>Düşenler {data.length}</Text>
        </View>
        </TouchableOpacity>
       
      </View>
      <FlatList
        style={styles.notificationList}
        enableEmptySections={true}
        data={data}
        keyExtractor={(item) => {
          return item.id;
        }}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity onPress={() => navigation.navigate(screenName, {
              name: `${item.name}`,
              datatime: `${item.datatime}`,
              icon: `${item.icon}`,
              price: `${item.price}`,
              fullname: `${item.fullname}`,
              market: `${item.market}`,
              marketlink: `${item.marketlink}`,
            })}>
              <View style={styles.notificationBox} >
                <Image style={styles.icon}
                  source={{ uri: item.icon }} />
                <Text style={styles.name}>{item.name}</Text>
                <Text style={styles.name}>{item.datatime}</Text>
                <Text style={styles.name}>{item.price}</Text>
                <Ionicons style={styles.iconfavori} name="chevron-forward-outline" size={20} color={"black"} />
              </View>
            </TouchableOpacity>

          )
        }}/>

      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              <Pressable style={styles.buttonClose} onPress={() => setModalVisible(!modalVisible)}>
                <Text style={styles.textStyle}>close</Text>
              </Pressable>
            </Text>
            <FlatList
                style={styles.notificationList}
                enableEmptySections={true}
                data={coinmarket}
                keyExtractor={(item) => {
                  return item.id;
                }}
                renderItem={({ item }) => {
                  return (
                    <TouchableOpacity   onPress={() => [navigation.navigate(screenMarket, {name: `${item.name}`,key: `${item.key}`}), setModalVisible(!modalVisible)]}>
                    <View style={styles.notificationBoxModal} >
                    <Image style={styles.iconmodal}
                      source={{uri: item.icon}}/>
                    <Text style={styles.namemodal}>{item.name}</Text>
                    <Ionicons style={styles.iconfavori} name="chevron-forward-outline" size={20} color={"black"}/>
                  </View>
                
                      </TouchableOpacity>
                  )
                }}/>
          </View>

        </Modal>
      </View>

    </SafeAreaView>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },

  notificationList: {
  },
  notificationBox: {
    padding: 15,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: 'black',
    flexDirection: 'row',
    borderRadius: 10,
    shadowColor: 'blue',
    shadowOpacity: .2,
    shadowOffset: {
      height:4,
      width:-4
    },
    elevation:3
  },

  icon: {
    width: 45,
    height: 45,
    marginRight: 5,
  },
  iconfavori: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  name: {
    flex: 1,
    flexDirection: 'row',
    marginStart: 5,
    paddingVertical: 10,
    fontSize: 13,
    color:'white',
    marginLeft: 10,
  },
  divider: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  },
    //CategoryBox
    CategoryBox: {
      marginBottom:2,
      flexDirection: 'row',
      backgroundColor:'black',
      shadowColor: 'blue',
      shadowOpacity: .2,
      shadowOffset: {
        height:4,
        width:-4
    },
    elevation:3
    },
    CategoryBoxname: {
      flex: 1,
      paddingVertical: 10,
      fontSize: 13,
      alignItems: 'center',
      justifyContent: 'center',
    },
  // Modal
  modalView: {
    flex: 1,
    borderTopEndRadius: 50,
    borderTopStartRadius: 50,
    marginTop: 300,
    width: Dimensions.get("window").width,
    backgroundColor: "#DCDCDC",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  buttonClose: {
    backgroundColor: "red",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  notificationBoxModal:{
    padding: 5,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    borderTopEndRadius: 20,
    borderBottomEndRadius: 20,
  },
  iconmodal:{
    marginStart:1,
    width: 45,
    height: 45,
  },
  namemodal:{
    flex:1,
    marginStart: 5,
    paddingVertical: 10,
    fontSize: 15,
    color: "black",
    marginLeft: 10,
  },
});

export default Allcoin;
import React from "react";
import { Text, View, Image, StyleSheet} from 'react-native';
import { useRoute } from "@react-navigation/native";

const DetailsPage = (datetime) => {
  const route = useRoute();

  if(route.params.date<1){
    datetime = "Yayınlandı"
  }
  else{
    datetime = route.params.date + " Kaldı"
  }
  return (
    <View style={styles.container}>
      <View style={styles.notificationList}>
        <View style={styles.notificationBox}>
          <Image style={styles.icon} source={{ uri: `${route.params.src}` }} />
          <View>
            <Text style={styles.name}>{route.params.name} <Text style={{  fontSize: 10, }}>({route.params.name})</Text></Text>
            <Text style={styles.name}>{datetime}</Text>
          </View>
        </View>
        <View style={[styles.notificationBoxContent]}>
          <View>
            <Text style={styles.name}>
            {route.params.description}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: 'black',
  },
  notificationList: {
    backgroundColor: 'black',
  },
  notificationBox: {
    padding: 15,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: 'black',
    flexDirection: 'row',
    shadowColor: 'blue',
    shadowOpacity: .2,
    shadowOffset: {
      height:4,
      width:-4
    },
    elevation:3

  },
  notificationBoxContent: {
    padding: 10,
    marginBottom: 1,
  },
  icon: {
    width: 60,
    height: 60,

  },
  name: {
    marginStart: 10,
    color: "white",
    marginLeft: 30,
    fontSize: 15,
  },
});

export default DetailsPage;

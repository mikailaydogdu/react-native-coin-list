import React from 'react';
import {View, Text, Image, StyleSheet, StatusBar} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
 
const data = [
  {
    title: 'Başlık 1',
    text: 'Açıklama.\nyusufpamukcu.com',
    bg: '#59b2ab',
    image: "https://www.seekpng.com/png/detail/193-1936873_bitcoin-btc-icon-bitcoin-logo-necklace-circle-charm.png"
  },
  {
    title: 'Başlık 2',
    text: 'Other cool stuff',
    bg: '#febe29',
    image: "https://www.seekpng.com/png/detail/193-1936873_bitcoin-btc-icon-bitcoin-logo-necklace-circle-charm.png"
  },
  {
    title: 'Başlık 3',
    text: "Açıklama\n\nLorem ipsum bla bla bla",
    bg: '#22bcb5',
    image: "https://www.seekpng.com/png/detail/193-1936873_bitcoin-btc-icon-bitcoin-logo-necklace-circle-charm.png"
  },
];
 
 
export default class AppIntroSliderPage extends React.Component {
  _renderItem = ({item}) => {
    return (
      <View
        style={[
          styles.slide,
          {
            backgroundColor: item.bg,
          },
        ]}>
        <Text style={styles.title}>{item.title}</Text>
        <Image source={item.image} style={styles.image} />
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
    
  };
 
  _keyExtractor = (item) => item.title;
 
  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar translucent backgroundColor="transparent" />
        <AppIntroSlider
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          data={data}
        />
      </View>
    );
  }
}
 
 
const styles = StyleSheet.create({
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'blue',
  },
  image: {
      flex:1,
    width: 320,
    height: 320,
    marginVertical: 32,
  },
  text: {
    color: 'rgba(255, 255, 255, 0.8)',
    textAlign: 'center',
  },
  title: {
    fontSize: 22,
    color: 'white',
    textAlign: 'center',
  },
});
import React, { Component, useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import coinFavori from '../favori'
import { useNavigation } from '@react-navigation/native';

const FavoriPage = ({ screenName}) => {
  const navigation = useNavigation();
  screenName = "Details"
  
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        style={styles.notificationList}
        enableEmptySections={true}
        data={coinFavori}
        keyExtractor={(item) => {
          return item.id;
        }}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity onPress={() => navigation.navigate(screenName, {
              name: `${item.name}`,
              date: `${item.date}`,
              src: `${item.src}`,
              description: `${item.description}`,
            })}>

              <View style={styles.notificationBox} >
                <Image style={styles.icon}
                  source={{ uri: item.icon }} />
                <Text style={styles.name}>{item.name}</Text>
                <Text style={styles.name}>{item.datatime}</Text>
                <Text style={styles.iconfavori}>{item.price}</Text>
              </View>
            </TouchableOpacity>

          )
        }} />
    </SafeAreaView>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },

  notificationList: {
    padding: 10,
  },
  notificationBox: {
    padding: 15,
    marginTop: 5,
    marginBottom: 5,
    marginLeft:5,
    marginRight:5,
    backgroundColor: 'black',
    flexDirection: 'row',
    borderRadius: 10,
    shadowColor: 'blue',
    shadowOpacity: 1,
    shadowOffset: {
      height:4,
      width:-4
    },
    elevation:10
  },
  icon: {
    width: 45,
    height: 45,
    marginRight: 5,
  },
  iconfavori: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    paddingHorizontal: 10,
    paddingVertical: 10,
    color:'white'
  },
  name: {
    flex: 1,
    flexDirection: 'row',
    marginStart: 5,
    paddingVertical: 10,
    fontSize: 13,
    color: "white",
    marginLeft: 10,
  },
  divider: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  }
});

export default FavoriPage;
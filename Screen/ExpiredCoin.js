import React, { Component, useState, useEffect, useNativeDriver } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
  Pressable,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Ionicons, MaterialCommunityIcons  } from '@expo/vector-icons'
import coinData from '../Data'
import { format } from "date-fns";


const ExpiredCoinPage = ({ screenName }) => {
  const [liked, setLiked] = useState(false);
  const navigation = useNavigation();
  screenName = "Details"

  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getCoin = async () => {
    try {
      const response = await fetch('{domain}');
      const json = await response.json();
      setData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getCoin();
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        style={styles.notificationList}
        enableEmptySections={true}
        data={data}
        keyExtractor={(item) => {
          return item.id;
        }}
        renderItem={({ item }) => {
          const date = new Date(item.date);
          const a = format(date, 'dd');
          const b = format(Date.now(), 'dd');
          const datetime =a-b;
          if(datetime<1){
            return (
                <TouchableOpacity onPress={() => navigation.navigate(screenName, {
                  name: `${item.name}`,
                  date: `${datetime}`,
                  src: `${item.src}`,
                  description: `${item.description}`,
                })}>
    
                  <View style={styles.notificationBox} >
                    <Image style={styles.icon}
                      source={{ uri: item.src }} />
                    <Text style={styles.name}>{item.name}</Text>
                    <Text style={styles.name}>{datetime} gün önce</Text>
                    <Text style={styles.iconfavori}>{item.price}</Text>
                    <TouchableOpacity style={styles.iconfavori} onPress={() => [item.id,setLiked((isLiked) => !isLiked)]}>
                      <Ionicons
                          name={liked ? "star" : "star-outline"}
                          size={20}
                          color={liked ? "blue" : "white"} />
                      </TouchableOpacity>
                  </View>
                  <View style={[styles.notificationBox, {marginTop:-5, borderRadius:0,shadowOffset: {height: -2,width: 2},elevation: 3}]} >
                         <Text style={styles.name}>{item.description}</Text>
                    </View>
                </TouchableOpacity>
              )
          }else{
          
       
        }

        }} />
    </SafeAreaView>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },

  notificationList: {
    padding: 15,
  },
  notificationBox: {
    padding: 15,
    marginBottom: 5,
    marginTop:5,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: 'black',
    flexDirection: 'row',
    borderRadius: 10,
    shadowColor: 'blue',
    shadowOpacity: 1,
    shadowRadius:10,
    shadowOffset: {
      height: 2,
      width: -2
    },
    elevation: 3
  },
  icon: {
    width: 45,
    height: 45,
    marginRight: 5,
  },
  iconfavori: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    paddingHorizontal: 10,
    paddingVertical: 10,
    color: 'white'
  },
  name: {
    flex: 1,
    flexDirection: 'row',
    marginStart: 5,
    paddingVertical: 10,
    fontSize: 13,
    color: "white",
    marginLeft: 10,
  },
  divider: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  }
});

export default ExpiredCoinPage;
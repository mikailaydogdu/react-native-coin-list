import React, { useState, useEffect } from 'react';
import { render } from 'react-dom';
import { FlatList, Text } from 'react-native';
import { View, StyleSheet, ScrollView, Dimensions, Image, TouchableOpacity, SafeAreaView } from 'react-native';
import ExpiredPage from '../Expired';
import { format } from "date-fns";
import { useNavigation } from '@react-navigation/native';
import moment from "moment";
import BigSlider from 'react-native-big-slider'
const { width } = Dimensions.get('window');
import { Button, Snackbar } from 'react-native-paper';

const IndexPage = ({ screenName, screenHome, screenExpired }) => {
  const [visible, setVisible] = React.useState(false);
  const onToggleSnackBar = () => setVisible(!visible);
  const onDismissSnackBar = () => setVisible(false);

  const navigation = useNavigation();
  screenName = "Details"
  screenHome = 'HomeTabs'
  screenExpired ='HomeTabs', { name: 'Expired' };

  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const getCoin = async () => {
    try {
      const response = await fetch('{domain}');
      const json = await response.json();
      setData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }
  useEffect(() => {
    getCoin();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
       <View style={styles.body}>
      <Button onPress={onToggleSnackBar}>{visible ? 'Hide' : 'Show'}</Button>
      <Snackbar
      style={{zIndex: -1,marginTop:100 }}
        visible={visible}
        onDismiss={onDismissSnackBar}
        action={{
          label: 'Undo',
          onPress: () => {onToggleSnackBar(visible ? 'Hide' : 'Show')},
        }}>
        Hey there! I'm a Snackbar.
      </Snackbar>
      </View>
      <View style={styles.body}>
        <Text style={styles.headertitle}>Bugün</Text>
        <Text style={styles.a}></Text>
        <FlatList
          pagingEnabled={true}
          horizontal={true}
          snapToAlignment={"center"}
          data={data}
          enableEmptySections={true}
          keyExtractor={(item) => {
            return item.id;
          }}
          renderItem={({ item }) => {
            const date = new Date(item.date);
            const a = format(date, 'dd');
            const b = format(Date.now(), 'dd');
            const datetime = a - b;

            const datehour = new Date(item.date);
            const ahour = moment(datehour);
            const bhour = moment();
            const datetimehour = ahour.diff(bhour, 'days');
            if (datetime === 0) {
              return (
                <TouchableOpacity onPress={() => navigation.navigate(screenName, {
                  name: `${item.name}`,
                  date: `${datetime}`,
                  src: `${item.src}`,
                  description: `${item.description}`,
                })}>

                  <View style={styles.view} >
                    <Image style={styles.icon} source={{ uri: item.src }} />
                    <Text style={styles.text}>{item.name}</Text>
                    <Text style={styles.text}>{datetimehour} gün kaldı </Text>
                  </View>
                </TouchableOpacity>
              )
            } else {
             return <Text style={{color:'white'}}> Boş </Text>;
            }
            
          }} />
         
      </View>

      <View style={styles.body}>
        <Text style={styles.headertitle}>Yakında</Text>
        <Text style={styles.a}></Text>
        <TouchableOpacity onPress={() => navigation.navigate(screenHome)}>
        <Text style={{marginStart:'80%', color:'blue'}}>View all</Text>
        </TouchableOpacity>
        <FlatList
          pagingEnabled={true}
          horizontal={true}
          snapToAlignment={"center"}
          data={data}
          enableEmptySections={true}
          keyExtractor={(item) => {
            return item.id;
          }}
          renderItem={({ item }) => {
            const a = moment(item.date);
            const b = moment();
            const datetime = a.diff(b, 'days');
            if (datetime < 1) {

            } else {

              return (
                <TouchableOpacity onPress={() => navigation.navigate(screenName, {
                  name: `${item.name}`,
                  date: `${datetime}`,
                  src: `${item.src}`,
                  description: `${item.description}`,
                })}>

                  <View style={styles.view} >
                    <Image style={styles.icon} source={{ uri: item.src }} />
                    <Text style={styles.text}>{item.name}</Text>
                    <Text style={styles.text}>{datetime} kaldı</Text>
                  </View>
                </TouchableOpacity>
              )
            }
          }} />
      </View>
      <View style={styles.body}>
        <Text style={styles.headertitle}>Yayınlanananlar</Text>
        <Text style={styles.a}></Text>
        <TouchableOpacity onPress={() => navigation.navigate(screenHome)}>
        <Text style={{marginStart:'80%', color:'blue'}}>View all</Text>
        </TouchableOpacity>
        <FlatList
          horizontal={true}
          snapToAlignment={"center"}
          data={data}
          enableEmptySections={true}
          keyExtractor={(item) => {
            return item.id;
          }}
          renderItem={({ item }) => {
            const a = moment(item.date);
            const b = moment();
            const datetime = b.diff(a, 'days');
            if (datetime < 1) {

            } else {
              return (
                <TouchableOpacity onPress={() => navigation.navigate(screenName, {
                  name: `${item.name}`,
                  date: `${datetime} önce`,
                  src: `${item.src}`,
                  description: `${item.description}`,
                })}>

                  <View style={styles.view} >
                    <Image style={styles.icon} source={{ uri: item.src }} />
                    <Text style={styles.text}>{item.name}</Text>
                    <Text style={styles.text}>{datetime} önce</Text>
                  </View>
                </TouchableOpacity>
              )
            }
          }} />
      </View>
    </SafeAreaView>
  );

}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: 'black'
  },
  body: {
    marginBottom: 20,
  },
  view: {
    backgroundColor: 'black',
    width: 100,
    margin: 15,
    height: 100,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'blue',
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 4,
      width: -4
    },
    elevation: 20
  },
  icon: {
    margin: 5,
    width: 30,
    height: 30,
  },
  text: {
    fontSize: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  headertitle: {
    color: 'blue',
    fontSize: 20,
    marginLeft: 10,
    marginBottom: 1,
  },
  a: {
    height: 3,
    width: 30,
    marginLeft: 10,
    marginTop:15,
    backgroundColor: 'blue'
  }
});
export default IndexPage;

import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import ProfilePage from '../components/profile';
import HelpPage from '../components/Help'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import SettingsPage from './Settings';


function HelpScreen({ navigation }) {
  return (
    <HelpPage/>
  );
}

function ProfileScreen({ navigation }) {
  return (
    <ProfilePage/>
  );
}

const Tab = createMaterialTopTabNavigator();
const ProfileTabs = ({ navigation }) => {
  return (
    <Tab.Navigator
      screenOptions={() => ({
        tabBarStyle: {
          backgroundColor: 'black',
          height: 50,
        },
        tabBarActiveTintColor: 'blue',
        tabBarInactiveTintColor: 'white',
      })}
    >
      <Tab.Screen name="Profile Page" component={ProfileScreen}  />
      <Tab.Screen name="Help" component={HelpScreen}  />
      <Tab.Screen name="settingspage" component={SettingsPage}  />
    </Tab.Navigator>
  )
}

export default ProfileTabs;
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  Button,
  SearchBar,
  SafeAreaView,
  TouchableHighlight,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons'
import { color } from 'react-native-reanimated';

const DATA = [
  {id:1,  description:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", color:"#228B22", completed:1},
  {id:2,  description:"Aenean massa. Cum sociis natoque penatibus et magnis.",     color:"#FF00FF", completed:0},
  {id:3,  description:"nascetur ridiculus mus. Donec quam felis, ultricies dnec.", color:"#4B0082", completed:1},
  {id:4,  description:"Donec pede justo, fringilla vel, aliquet nec, vulputdate.", color:"#20B2AA", completed:0},
  {id:5,  description:"Nullam dictum felis eu pede mollis pretium. Integer tirr.", color:"#000080", completed:0},
  {id:6,  description:"ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas st.", color:"#FF4500", completed:1},
  {id:7,  description:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", color:"#FF0000", completed:0},
  {id:8,  description:"Maecenas nec odio et ante tincidunt tempus. Donec vitae .", color:"#EE82EE", completed:0},
  {id:9,  description:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", color:"#6A5ACD", completed:0},
]


const NotificationPage = ({ screenName, __getCompletedIcon }) => {
  const navigation = useNavigation();
  screenName = "Details"

  __getCompletedIcon = (item) => {
    if(item.completed == 1) {
      return "https://img.icons8.com/flat_round/64/000000/checkmark.png";
    } else {
      return "https://img.icons8.com/flat_round/64/000000/delete-sign.png";
    }
  }
  return (
    <View style={styles.container}>
      <FlatList
        style={styles.notificationList}
        enableEmptySections={true}
        data={DATA}
        keyExtractor={(item) => {
          return item.id;
        }}
        renderItem={({ item }) => {
          return (
              <View style={[styles.notificationBox, { borderLeftWidth:10, borderLeftColor:item.color}]} >
                <Image style={styles.image} source={{uri: __getCompletedIcon(item)}}/>
                <Text style={styles.name}>{item.description}</Text>
              </View>            
          )
        }} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: 'black',
  },
  notificationList: {
  },
  notificationBox: {
    flex:1,
    padding: 10,
    margin:10,
    marginBottom: 3,
    marginTop: 3,
    backgroundColor: 'black',
    flexDirection: 'row',

    borderTopStartRadius:10,
    borderBottomStartRadius:10,
  },
  name: {
    marginStart: 5,
    fontSize: 13,
    color: "white",
  },
  image:{
    width:25,
    height:25,
  },
});

export default NotificationPage;